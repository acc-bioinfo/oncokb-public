FROM ubuntu:16.04

WORKDIR /oncokb_prepare

RUN echo '{ "allow_root": true }' > /root/.bowerrc

RUN apt-get update && apt-get install -y \
  build-essential \
  git \
  openjdk-8-jdk \
  maven \
  git \
  nodejs \
  npm

RUN git clone https://github.com/oncokb/oncokb.git \
  && git clone https://github.com/oncokb/oncokb-public.git \
  && mv oncokb-public oncokb/web/public

RUN cd /oncokb_prepare/oncokb \
  && mvn clean install -DskipTests=true -P public

FROM openjdk:8-jre
COPY --from=0 /oncokb_prepare/oncokb/web/target/dependency/webapp-runner.jar /webapp-runner.jar
COPY --from=0 /oncokb_prepare/oncokb/web/target/*.war /app.war
# specify default command
ENTRYPOINT /usr/bin/java ${JAVA_OPTS} -jar /webapp-runner.jar ${WEBAPPRUNNER_OPTS} /app.war

